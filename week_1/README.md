![](https://img.shields.io/badge/Code-Python-informational?style=flat&logo=python&logoColor=white&color=2bbc8a)

<h1>Week 1 (7 Oct)</h1>

1) Write a function that returns the highest element of a list of integers

2) Write a function that returns only the odd elements of the list

3) Write a function that checks to see if a string is a palindrome

The python files above may be used a starting point, or you may build your own solutions from scratch.