print("Generating Random List of Numbers")
import random
#get me 50 random numbers between 0 and 1000
randomlist = random.sample(range(0, 1000), 50)
print(randomlist)
#make the randomness a list, I mean it might be already but this can't hurt right
list1 = randomlist
#filter using lambda functions and def had to google what they do and easiest ways to filter for odds
odds = list(filter(lambda x: x % 2 != 0, list1))
print("Selecting only odd numbers")
print(odds)
#trying to see how to output things differently
print("Odd numbers in new lines")
print(*odds, sep="\n")
